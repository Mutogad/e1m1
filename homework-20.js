"use strict";

//Упражнение 1
for (let a = 0; a<=20; a++) {
    if (a===2) continue;
    if (a===4) continue;
    if (a===6) continue;
    if (a===8) continue;
    if (a===10) continue;
    if (a===12) continue;
    if (a===14) continue;
    if (a===16) continue;
    if (a===18) continue;
    if (a===20) continue;

    console.log(a);
}

//Упражнение 2

let sum = 0;

for (let a = 0; a < 3; a++) {

    let number = +prompt (`Введите число`);

    if (isNaN (number)) {
        alert (`Вы ввели не число`);
        break;
    }
    sum += number;
}

alert ('Сумма:' +sum);

// Упражнение 3

function getNameOfMonth(month) {
    if (month === 0) return 'Январь';
    if (month === 1) return 'Февраль';
    if (month === 2) return 'Март';
    if (month === 3) return 'Апрель';
    if (month === 4) return 'Май';
    if (month === 5) return 'Июнь';
    if (month === 6) return 'Июль';
    if (month === 7) return 'Август';
    if (month === 8) return 'Сентябрь';
    if (month === 9) return 'Октябрь';
    if (month === 10) return 'Ноябрь';
    if (month === 11) return 'Декабрь';
}

for ( let month = 0; month < 12; month++) {
    let m = getNameOfMonth(month);

    if (month === 9) continue;

    console.log (m);
}

// Упражнение 4

// IIFE - немедленно вызываемая функция в себе, которая не может использоваться за пределами ее области видимости

// !function () {
//     alert ("Ифи");
// } ();         !- приводит функцию к выражению, как и любой унарный опрератор. Скобочки в конце - сразу приводят функцию в действие

// (function abcd() {
//     какой то код;
// }());          - еще один пример. возможны смещения двойных скобок в конце по бокам от закрывающей скобки